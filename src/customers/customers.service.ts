import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    return this.customerRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customerRepository.find();
  }

  async findOne(id: number) {
    return this.customerRepository.findOne({
      where: { id: id },
      relations: ['order'],
    });
    if (!Customer) {
      throw new NotfoundException();
    }
    return Customer;
  }

  update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const user = await this.customerRepository.findOneBy({ id: id });
    const updateCustomerDto = { ...customer, ...updateCustomerDto };
    return this.customerRepository.update(id, updateCustomerDto);
  }

  remove(id: number) {
    const user = await this.customerRepository.findOneBy({ id: id });
    return this.customerRepository.softRemove(user);
  }
}
