import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class CreateCustomerDto {
  @PrimaryGeneratedColomn()
  name: number;

  @column()
  age: number;

  @column()
  tel: string;

  @column()
  gender: string;
}
