class createOrderItemDto {
  customerId: number;
  amount: number;
}

export class CreateOrderDto {
  customerId: number;
  orderItem: CreateOrderItemDto[];
}
