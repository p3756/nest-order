import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderItem } from './entities/order-item';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/dto/create-product.dto';

@Module({
  imports[TypeOrmModule.forFeature([Order ,OrderItem ,Customer,Product])],
  controllers: [OrderController],
  providers: [OrderService]
})
export class OrderModule {}
