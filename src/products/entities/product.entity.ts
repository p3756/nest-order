import { OrderItem } from 'src/order/entities/order-item';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  price: number;

  @OneToMany(() => OrderItem, (OrderItem) => OrderItem.product)
  orderItem: OrderItem;
}
